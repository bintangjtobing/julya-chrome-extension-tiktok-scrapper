import Chart from "chart.js/auto";
document.addEventListener("DOMContentLoaded", function () {
  const scrapeButton = document.getElementById("scrapeButton");
  const sortOption = document.getElementById("sortOption");
  const messageDiv = document.getElementById("message");
  const dataDisplay = document.getElementById("dataDisplay");
  const downloadButton = document.getElementById("downloadButton");
  const daysInput = document.getElementById("daysInput");
  const fromDateInput = document.getElementById("fromDate");
  const toDateInput = document.getElementById("toDate");
  const chooseOptionButton = document.getElementById("chooseOptionButton");

  const scrapingOptionsModal = document.getElementById("scrapingOptionsModal");
  const startScrapingButton = document.getElementById("startScrapingButton");

  chooseOptionButton.addEventListener("click", function () {
    scrapingOptionsModal.style.display = "block";
  });

  startScrapingButton.addEventListener("click", function () {
    const selectedOption = document.querySelector(
      'input[name="scrapingOption"]:checked'
    ).value;
    const days = daysInput.value;
    const fromDate = fromDateInput.value;
    const toDate = toDateInput.value;

    // Perform scraping based on the selected option here

    // Close the modal
    scrapingOptionsModal.style.display = "none";
  });

  let scrapedData = null;

  function displayData(data) {
    dataDisplay.textContent = JSON.stringify(data, null, 2);
    messageDiv.textContent = "Data successfully scraped!";
    messageDiv.className = "success";
    scrapedData = data;
    downloadButton.disabled = false;
  }

  function downloadData(data) {
    const blob = new Blob([JSON.stringify(data, null, 2)], {
      type: "application/json",
    });
    const url = URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.href = url;
    link.download = "scraped_data.json";
    link.click();
    URL.revokeObjectURL(url);
  }

  scrapeButton.addEventListener("click", function () {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      const tab = tabs[0];
      // Periksa jika URL adalah URL TikTok
      if (tab.url && tab.url.startsWith("https://www.tiktok.com/")) {
        const tiktokRegex = /^https:\/\/www\.tiktok\.com\/@([^/]+)/;
        const match = tab.url.match(tiktokRegex);

        // Jika URL cocok dengan pola TikTok
        if (match) {
          const username = match[1]; // Ambil username dari URL TikTok
          // Sekarang Anda dapat melakukan scraping untuk profil pengguna ini
          // Panggil fungsi scraping dengan username sebagai parameter
          scrapeUserProfile(username);
        } else {
          messageDiv.textContent = "URL TikTok tidak valid.";
          messageDiv.className = "error";
        }
      } else {
        messageDiv.textContent = "Buka profil TikTok terlebih dahulu.";
        messageDiv.className = "error";
      }
    });
  });

  function scrapeUserProfile(username) {
    chrome.runtime.sendMessage(
      {
        action: "fetchData",
        url: `https://tokapi-mobile-version.p.rapidapi.com/v1/user/${username}`,
        params: {
          headers: {
            "Content-Type": "application/json",
            "x-rapidapi-key":
              "c71ad09bd8msh9e09b2e2fb7abffp104a1cjsn24452b70c2dd",
            "x-rapidapi-host": "tokapi-mobile-version.p.rapidapi.com",
          },
        },
      },
      (response) => {
        if (response.status === "success") {
          displayData(response.data);
          downloadButton.disabled = false;
        } else {
          messageDiv.textContent = "Terjadi kesalahan saat scraping data.";
          messageDiv.className = "error";
          downloadButton.disabled = true;
        }
      }
    );
  }

  const data = {
    labels: ["Label 1", "Label 2", "Label 3", "Label 4", "Label 5"],
    datasets: [
      {
        label: "Data",
        data: [10, 20, 30, 40, 50], // Ganti dengan data yang sesuai
        backgroundColor: "rgba(75, 192, 192, 0.2)",
        borderColor: "rgba(75, 192, 192, 1)",
        borderWidth: 1,
      },
    ],
  };

  const config = {
    type: "bar",
    data: data,
    options: {
      scales: {
        y: {
          beginAtZero: true,
        },
      },
    },
  };

  const myChart = new Chart(document.getElementById("myChart"), config);
});
