chrome.runtime.onInstalled.addListener(() => {
  console.log("TikTok Scraper Extension Installed");
});

// Contoh fungsi untuk melakukan request ke API
async function fetchDataFromApi(url, params) {
  try {
    // Anda perlu mengimplementasikan logika untuk memanggil API Tokapi di sini
    // Contoh menggunakan fetch:
    const response = await fetch(url, { method: "GET", headers: params });
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error fetching data:", error);
    return null;
  }
}

// Mendengarkan pesan dari popup.js
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.action === "fetchData") {
    fetchDataFromApi(request.url, request.params)
      .then((data) => sendResponse({ status: "success", data: data }))
      .catch((error) => sendResponse({ status: "error", error: error }));
    return true; // Menjaga channel komunikasi tetap terbuka untuk response asynchronous
  }
});
